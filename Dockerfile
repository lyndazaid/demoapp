# Utiliser l'image de base Node.js 
FROM node:16-alpine3.12 AS builder

WORKDIR /app

COPY . .

# Installation
RUN npm install --force
RUN npm run build -- --outputPath=./dist/out --configuration=production

# Deploiement de l'application
FROM nginx:alpine


COPY --from=builder /app/dist/out /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
